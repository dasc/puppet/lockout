class lockout (
    Enum['file', 'absent'] $nologin,
    String $maint_date,
    String $maint_start_time,
    String $maint_end_time,
    String $motd_message = '',
    Optional[Variant[String, Array[String]]] $motd_source = undef,
) {
    exec {'lockout daemon-reload':
        command     => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    file {'/usr/lib/systemd/system/persistent-nologin.service':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/lockout/usr/lib/systemd/system/persistent-nologin.service',
        notify => Exec['lockout daemon-reload'],
    }

    file { ['/etc/persistent-nologin', '/etc/nologin']:
        ensure  => $nologin,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('lockout/nologin.epp', {
            maint_date       => $maint_date,
            maint_start_time => $maint_start_time,
            maint_end_time   => $maint_end_time,
        }),
    }

    if $motd_message and $motd_message != '' {
        file { '/etc/motd':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => "${motd_message}\n",
        }
    } elsif $motd_source {
        file { '/etc/motd':
            ensure => 'file',
            owner  => 'root',
            group  => 'root',
            mode   => '0644',
            source => $motd_source,
        }
    } else {
        file { '/etc/motd':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('lockout/motd.epp', {
                maint_date       => $maint_date,
                maint_start_time => $maint_start_time,
                maint_end_time   => $maint_end_time,
            }),
        }
    }
}
